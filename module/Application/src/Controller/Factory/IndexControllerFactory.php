<?php
namespace Application\Controller\Factory;

use Application\Service\CarManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\IndexController;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $carManger = $container->get(CarManager::class);

        return new IndexController($entityManager, $carManger);
    }
}