<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\Car;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\CarManager
     */
    private $carManager;

    public function __construct($entityManager, $carManager)
    {
        $this->entityManager = $entityManager;
        $this->carManager = $carManager;
    }

    public function indexAction()
    {
        $cars = $this->entityManager->getRepository(Car::class)
            ->findAll();

        return $this->returnJson($cars, 'SUCCESS');
    }

    public function saveAction()
    {
        $body = $this->getRequest()->getContent();
        $input = json_decode($body, true);

        $car = $this->carManager->addNewCar($input);

        return $this->returnJson($car, 'SUCCESS');
    }

    protected function returnJson($data, $status)
    {
        return new JsonModel([
            'status' => $status,
            'data' => $data
        ]);
    }
}
