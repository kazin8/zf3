<?php
namespace Application\Service;

use Application\Entity\Car;
use Application\Entity\Mark;
use Application\Entity\Model;
use Zend\Filter\StaticFilter;

class CarManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addNewCar($data)
    {
        $car = new Car();
        $car->setName($data['name']);
        $car->setCreatedAt(date('Y-m-d H:i:s'));

        $mark = $this->entityManager->getRepository(Mark::class)
            ->find($data['mark_id']);
        $car->setMark($mark);

        $model = $this->entityManager->getRepository(Model::class)
            ->find($data['model_id']);
        $car->setModel($model);

        $this->entityManager->persist($car);

        $this->entityManager->flush();

        return $car;
    }
}