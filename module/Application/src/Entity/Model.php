<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="model")
 */
class Model
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    protected $id;

    /**
     * @ORM\Column(name="name")
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Mark", inversedBy="model")
     * @ORM\JoinColumn(name="mark_id", referencedColumnName="id")
     */
    private $mark;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /*
     * @return \Application\Entity\Mark
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Задает связанный пост.
     * @param \Application\Entity\Mark $mark
     */
    public function setMark($mark)
    {
        $this->mark = $mark;
        $mark->addModel($this);
    }
}