<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="car")
 */
class Car implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    protected $id;

    /**
     * @ORM\Column(name="name")
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Mark", inversedBy="cars")
     * @ORM\JoinColumn(name="mark_id", referencedColumnName="id")
     */
    private $mark;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Model", inversedBy="cars")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     */
    private $model;

    /**
     * @ORM\Column(name="created_at")
     */
    protected $createdAt;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /*
     * @return \Application\Entity\Mark
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Задает связанный пост.
     * @param \Application\Entity\Mark $mark
     */
    public function setMark($mark)
    {
        $this->mark = $mark;
    }

    /*
     * @return \Application\Entity\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Задает связанный пост.
     * @param \Application\Entity\Model $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    public function jsonSerialize()
    {
        return array(
            'name' => $this->getName(),
            'id'=> $this->getId(),
            'created_at' => $this->getCreatedAt(),
            'mark' => $this->getMark()->getName(),
            'model' => $this->getModel()->getName()
        );
    }
}